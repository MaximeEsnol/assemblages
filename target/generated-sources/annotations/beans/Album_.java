package beans;

import beans.Photo;
import beans.User;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-05T15:53:25")
@StaticMetamodel(Album.class)
public class Album_ { 

    public static volatile SingularAttribute<Album, User> forUser;
    public static volatile SingularAttribute<Album, User> owner;
    public static volatile SingularAttribute<Album, String> name;
    public static volatile SingularAttribute<Album, String> link;
    public static volatile SingularAttribute<Album, String> description;
    public static volatile SingularAttribute<Album, Integer> id;
    public static volatile SingularAttribute<Album, Integer> publicity;
    public static volatile SingularAttribute<Album, Date> creationDate;
    public static volatile ListAttribute<Album, Photo> photos;

}