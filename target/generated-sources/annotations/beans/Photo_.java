package beans;

import beans.Album;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-05T15:53:25")
@StaticMetamodel(Photo.class)
public class Photo_ { 

    public static volatile SingularAttribute<Photo, String> path;
    public static volatile SingularAttribute<Photo, Album> album;
    public static volatile SingularAttribute<Photo, String> name;
    public static volatile SingularAttribute<Photo, String> location;
    public static volatile SingularAttribute<Photo, Integer> id;

}