<%-- 
    Document   : noaccess
    Created on : Mar 5, 2020, 10:25:43 AM
    Author     : MEOBL79
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Geen toegang - Assemblages</title>
    </head>
    <body>
        <h1>Sorry, u heeft geen toegang tot deze pagina.</h1>
    <c:if test="${not empty infoMessage}">
        <p><c:out value="${infoMessage}"/></p>
    </c:if>
</body>
</html>
