<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<div class="albums">
    <c:forEach items="${albums}" var="album">
        <div class="album" id="album-<c:out value="${album.id}"/>">
            <img class="define-icon" src="files/icons/book-open.svg"/>
            <div class="cover-image" id="album-<c:out value="${album.id}"/>" style="background-image: url('files/photos/static/loading.gif'); background-size: contain; background-repeat: no-repeat;">

            </div>
            <div class="content">
                <h2>
                    <c:out value="${album.name}"/> 
                    <a href="dashboard/album/edit?album=<c:out value="${album.id}"/>">
                        <img src="files/icons/edit-2.svg"/>
                    </a>
                        
                    <a class="background circle" href="dashboard/photos/add?album=<c:out value="${album.id}"/>">
                        <img src="files/icons/plus-colored.svg"/>
                    </a>
                </h2>
                <p>Aangemaakt: <c:out value="${album.creationDate}"/></p>
            </div>
        </div>
    </c:forEach>
</div>