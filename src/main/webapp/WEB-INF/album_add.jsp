<%-- 
    Document   : album_add
    Created on : Mar 3, 2020, 3:34:07 PM
    Author     : MEOBL79
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Album aanmaken | Assemblages</title>
        <link rel="stylesheet" href="../../files/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../../files/css/datepicker.min.css"/>
        <link rel="stylesheet" href="../../files/css/styles.css"/>
    </head>
    <body>
        <c:import url="imports/header.jsp"/>
        <div class="container">
            <header class="signedin">
                <h1>Nieuw album aanmaken</h1>
            </header>

            <c:if test="${not empty infoMessage}">
                <div class="infoMessage error">
                    <p><c:out value="${infoMessage}"/></p>
                </div>
            </c:if>

            <form method="POST" action="new" name="add">
                <div class="form-group">
                    <label for="name">
                        Albumnaam
                    </label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Hoe heet dit album?" required/>
                </div>

                <div class="form-group">
                    <label for="description">
                        Omschrijving
                    </label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Waarover gaat dit fotoalbum?" required/>
                </div>

                <div class="form-group">
                    <label for="target">
                        Email adres van de persoon voor wie dit album bedoelt is
                    </label>
                    <input type="email" class="form-control" id="target" name="target" placeholder="Voor wie is dit fotoalbum?"/>
                    <small id="targetHelp" class="form-text text-muted">Als de persoon geen Assemblages-account heeft, mag u nog steeds zijn of haar email invullen. Er wordt dan automatisch een 
                        link gegenereert die aan de persoon doorgestuurd wordt.</small>
                </div>

                <div class="form-group">
                    <p>Maak dit fotoalbum:</p>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="private" name="publicity" class="custom-control-input" value="private" required/>
                        <label class="custom-control-label" for="private">privé, enkel ik en de persoon voor wie het bedoelt is kan het bekijken.</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="withlink" name="publicity" class="custom-control-input" value="withlink"/>
                        <label class="custom-control-label" for="withlink">onzichtbaar, met een deelbare link. Iedereen die over de deelbare link beschikt kan het bekijken.</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="public" name="publicity" class="custom-control-input" value="public"/>
                        <label class="custom-control-label" for="public">publiek, iedereen kan het fotoalbum zien vanaf de Assemblages website.</label>
                    </div>
                </div>

                <div class="form-group link">
                    <label for="link">Deelbare link</label>
                    <p>localhost:8090/Assemblages/album?view=<input type="text" readonly id="link" class="form-control" name="link" value="" placeholder="Link genereren..."/></p>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Album aanmaken</button>
                </div>
            </form>
        </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../../files/js/bootstrap.min.js"></script>
        <script src="../../files/js/assemblages.js"></script>
        <script>
            let radios = document.add.publicity;
            let link = document.querySelector(".form-group.link");
            let link_shown = false;
            for (let i = 0; i < radios.length; i++) {
                radios[i].addEventListener("change", function () {
                    if (this.value === "withlink" && !link_shown) {
                        console.log(link);
                        link.style.display = "block";
                        generateUniqueLink(document.getElementById('link'));
                        link_shown = true;
                    } else if (this.value !== "withlink" && link_shown) {
                        link.style.display = "none";
                        document.getElementById('link').value = "";
                        link_shown = false;
                    }
                });
            }
        </script>
    </body>
</html>
