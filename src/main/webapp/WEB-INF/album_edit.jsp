<%-- 
    Document   : album_edit
    Created on : Mar 5, 2020, 10:04:46 AM
    Author     : MEOBL79
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Album bewerken | Assemblages</title>
        <link rel="stylesheet" href="../../files/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../../files/css/datepicker.min.css"/>
        <link rel="stylesheet" href="../../files/css/styles.css"/>
    </head>
    <body>
        <c:import url="imports/header.jsp"/>
        <div class="container">
            <header class="signedin">
                <h1>"<c:out value="${album.name}"/>" bewerken</h1>
            </header>

            <c:if test="${not empty infoMessage}">
                <div class="infoMessage error">
                    <p><c:out value="${infoMessage}"/></p>
                </div>
            </c:if>
            <form method="POST" action="edit" name="add">
                <input hidden type="text" name="id" id="id" value="<c:out value="${album.id}"/>"/>
                <div class="form-group">
                    <label for="name">
                        Albumnaam
                    </label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Hoe heet dit album?" value="<c:out value="${album.name}"/>" required/>
                </div>

                <div class="form-group">
                    <label for="description">
                        Omschrijving
                    </label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Waarover gaat dit fotoalbum?" value="<c:out value="${album.description}"/>" required/>
                </div>

                <div class="form-group">
                    <label for="target">
                        Email adres van de persoon voor wie dit album bedoelt is
                    </label>
                    <c:choose>
                        <c:when test="${album.forUser.id eq sessionScope.user.id}">
                            <input type="email" class="form-control" id="target" name="target" placeholder="Voor wie is dit fotoalbum?"/>
                        </c:when>
                        <c:otherwise>
                            <input type="email" class="form-control" id="target" name="target" placeholder="Voor wie is dit fotoalbum?" value="<c:out value="${album.forUser.email}"/>"/>
                        </c:otherwise>
                    </c:choose>
                    <small id="targetHelp" class="form-text text-muted">Als de persoon geen Assemblages-account heeft, mag u nog steeds zijn of haar email invullen. Er wordt dan automatisch een 
                        link gegenereert die aan de persoon doorgestuurd wordt.</small>
                </div>

                <div class="form-group">
                    <p>Maak dit fotoalbum:</p>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="private" 
                               <c:if test="${album.publicity eq 0}">
                                   checked
                               </c:if>
                               name="publicity" class="custom-control-input" value="private" required/>
                        <label class="custom-control-label" for="private">privé, enkel ik en de persoon voor wie het bedoelt is kan het bekijken.</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" 
                               <c:if test="${album.publicity eq 1}">
                                   checked
                               </c:if>
                               id="withlink" name="publicity" class="custom-control-input" value="withlink"/>
                        <label class="custom-control-label" for="withlink">onzichtbaar, met een deelbare link. Iedereen die over de deelbare link beschikt kan het bekijken.</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" 
                               <c:if test="${album.publicity eq 2}">
                                   checked
                               </c:if>
                               id="public" name="publicity" class="custom-control-input" value="public"/>
                        <label class="custom-control-label" for="public">publiek, iedereen kan het fotoalbum zien vanaf de Assemblages website.</label>
                    </div>
                </div>

                <div class="form-group link">
                    <label for="link">Deelbare link</label>
                    <p>localhost:8090/Assemblages/album?view=<input type="text" readonly id="link" class="form-control" name="link" 
                                                                    <c:if test="${not empty album.link}">
                                                                        value="<c:out value="${album.link}"/>"   
                                                                        data-update="false"
                                                                    </c:if>
                                                                    placeholder="Link genereren..."/></p>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Wijzigingen opslaan</button>
                </div>
            </form>
        </div>


        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../../files/js/bootstrap.min.js"></script>
        <script src="../../files/js/assemblages.js"></script>
        <script>
            let radios = document.add.publicity;
            let link = document.querySelector(".form-group.link");
            let link_shown = false;
            let linkInput = document.getElementById('link');
            <c:if test="${album.publicity eq 1}">
            link.style.display = "block";
            </c:if>
            for (let i = 0; i < radios.length; i++) {
                radios[i].addEventListener("change", function () {
                    if (this.value === "withlink" && !link_shown) {
                        console.log(link);
                        link.style.display = "block";
                        if (linkInput.getAttribute("data-update") !== "false") {
                            generateUniqueLink(linkInput);
                        }
                        link_shown = true;
                    } else if (this.value !== "withlink" && link_shown) {
                        link.style.display = "none";
                        if (linkInput.getAttribute("data-update") !== "false") {
                            document.getElementById('link').value = "";
                        }
                        link_shown = false;
                    }
                });
            }
        </script>
    </body>
</html>
