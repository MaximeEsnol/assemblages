<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="websiteName" value="http://localhost:8090/Assemblages/"/>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<c:out value="${websiteName}"/>index.jsp">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Photographers</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li>
        </ul>

    </div>
    <div class="mx-auto order-0">
        <a class="navbar-brand mx-auto" href="/">Assemblages</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
            <c:choose>
                <c:when test="${empty user}">
                    <a class="nav-link" href="<c:out value="${websiteName}"/>authenticate">Aanmelden of Registreren</a>
                </c:when>
                <c:otherwise>
                    <a class="nav-link" href="<c:out value="${websiteName}"/>authenticate?signout">Afmelden</a>
                </c:otherwise>
            </c:choose>
            </li>
        </ul>
    </div>
</nav>
