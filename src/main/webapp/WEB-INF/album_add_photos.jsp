<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Foto's toevoegen aan een album | Assemblages</title>
        <link rel="stylesheet" href="../../files/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="../../files/css/datepicker.min.css"/>
        <link rel="stylesheet" href="../../files/css/styles.css"/>
    </head>
    <body>
        <c:import url="imports/header.jsp"/>

        <div class='container'>
            <h1>Voeg foto's toe aan <b><c:out value="${album.name}"/></b></h1>
            <c:out value="${test}"/>
            <form method="post" action="add" enctype="multipart/form-data">
                <input type='text' hidden id="album" name="album" value="<c:out value="${param.album}"/>"/>
                <div class="form-group upload">
                    <label for="images">Klik om foto's toe te voegen</label>
                    <input type="file" multiple class="form-control-file" name="photos" id="photos" onchange='showPreviews("photos")'/>
                </div>

                <div id="upload_confirmation">
                    <p>U gaat de volgende foto's uploaden.</p>
                </div>
                
                <button type='submit' class="btn btn-primary">Uploaden</button>
            </form>
            <div id='photos_selected'>

            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="../../files/js/bootstrap.min.js"></script>
        <script src="../../files/js/assemblages.js"></script>
        <script>
                        let elems = document.getElementById('photos_selected');
                        function showPreviews(id) {
                            let elem = document.getElementById(id);

                            for (let i = 0; i < elem.files.length; i++) {
                                console.log("kak");
                                let reader = new FileReader();
                                reader.onload = function (e) {
                                    console.log("yes");
                                    let img = document.createElement('div');
                                    img.setAttribute('class', 'image-preview');
                                    img.setAttribute('data-id', i);
                                    img.style.backgroundImage = "url(" + e.target.result + ")";
                                    elems.appendChild(img);
                                };

                                reader.readAsDataURL(elem.files[i]);
                            }

                        }
        </script>
    </body>
</html>
