<%-- 
    Document   : authenticate
    Created on : Feb 21, 2020, 10:17:00 AM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Aanmelden of registreren | Assemblages</title>
        <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="files/css/datepicker.min.css"/>
        <link rel="stylesheet" href="files/css/styles.css"/>
    </head>
    <body>
        <c:import url="/WEB-INF/imports/header.jsp"/>
        <div class="container">
            <c:choose>
                <c:when test="${empty sessionScope.user}">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1>Aanmelden bij Assemblages</h1>
                            <c:if test="${not empty infoMessage}">
                                <div class="infoMessage error">
                                    <p>Er trad een fout op: <c:out value="${infoMessage}"/></p>
                                </div>
                            </c:if>
                            <c:if test="${not empty successMessage}">
                                <div class="infoMessage success">
                                    <p><c:out value="${successMessage}"/></p>
                                </div>
                            </c:if>
                            <ul class="nav nav-tabs">
                                <li class="nav-item active">
                                    <a class="nav-link" data-toggle="tab" href="#signin-tab">
                                        Aanmelden
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#register-tab">
                                        Account aanmaken
                                    </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div id="signin-tab" class="tab-pane fade in active">
                                    <form action="authenticate" method="post">
                                        <div class="form-group">
                                            <label for="email">
                                                Email-adres
                                            </label>
                                            <input type="email" class="form-control" name="email" id="email" placeholder="voorbeeld@domein.com" required="true"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="password">
                                                Wachtwoord
                                            </label>
                                            <input type="password" class="form-control" name="password" id="password" placeholder="Uw wachtwoord" required="true"/>
                                        </div>

                                        <button type="submit" id="confirmSignIn" class="btn btn-primary" name="confirmSignIn">
                                            Aanmelden
                                        </button>
                                    </form>
                                </div>

                                <div id="register-tab" class="tab-pane fade in">
                                    <form action="authenticate" method="post">
                                        <div class="form-group">
                                            <label for="registerEmail">
                                                Kies uw email adres
                                            </label>
                                            <input type="email" class="form-control" name="registerEmail" id="registerEmail" placeholder="voorbeeld@domein.com" required="true"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="registerUsername">
                                                Kies uw Assemblages-gebruikersnaam
                                            </label>
                                            <input type="registerUsername" class="form-control" name="registerUsername" id="registerUsername" placeholder="Uw gebruikersnaam" required="true"/>
                                            <small class="form-text text-muted">
                                                Uw gebruikersnaam mag enkel uit letters en cijfers bestaan, en mag maximaal 30 tekens bevatten.
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="registerPassword">
                                                Kies een sterk wachtwoord
                                            </label>
                                            <input type="password" class="form-control" name="registerPassword" id="registerPassword" placeholder="Uw wachtwoord" required="true"/>
                                            <small class="form-text text-muted">
                                                Uw wachtwoord moet bestaan uit minstens één kleine letter, één hoofdletter en één cijfer. Uw wachtwoord moet minstens 8 tekens lang zijn.
                                            </small>
                                        </div>
                                        <div class="form-group">
                                            <label for="birthdate">Uw geboortedatum</label>
                                            <input type="text" data-toggle="datepicker" name="birthdate" id="birthdate" class="form-control">
                                            <div data-toggle="datepicker"></div>
                                        </div>
                                        <button type="submit" class="btn btn-primary" name="confirmRegister" id="confirmRegister">
                                            Account aanmaken
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="photo-fill-container">
                                <img src='files/photos/static/woman_taking_photo.jpg'/>
                            </div>
                        </div>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="content-warning">
                        <p>
                            <c:out value="${sessionScope.user.username}"/>, 
                            U bent al aangemeld.
                        </p>
                    </div>
                </c:otherwise>
            </c:choose>


        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="files/js/bootstrap.min.js"></script>
        <script src="files/js/datepicker.min.js"></script>
        <script>
            $('[data-toggle="datepicker"]').datepicker({
                format: 'yyyy-mm-dd'
            });
        </script>
    </body>
</html>
