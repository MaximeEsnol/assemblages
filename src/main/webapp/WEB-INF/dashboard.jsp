<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Uw dashboard | Assemblages</title>
        <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="files/css/datepicker.min.css"/>
        <link rel="stylesheet" href="files/css/styles.css"/>
    </head>
    <body>
        <c:import url="/WEB-INF/imports/header.jsp"/>
        <div class="container">
            <c:choose>
                <c:when test="${not empty sessionScope.user}">
                    <div id="db-loader" class="page-loader appearing">
                        <div class="content">
                            <div class="sk-cube-grid">
                                <div class="sk-cube sk-cube1"></div>
                                <div class="sk-cube sk-cube2"></div>
                                <div class="sk-cube sk-cube3"></div>
                                <div class="sk-cube sk-cube4"></div>
                                <div class="sk-cube sk-cube5"></div>
                                <div class="sk-cube sk-cube6"></div>
                                <div class="sk-cube sk-cube7"></div>
                                <div class="sk-cube sk-cube8"></div>
                                <div class="sk-cube sk-cube9"></div>
                            </div>

                            <p>Uw fotoalbums zijn aan het laden...</p>
                        </div>
                    </div>
                    <header class="signedin">
                        <h1>Uw dashboard</h1>
                        <p>Aangemeld als <c:out value="${sessionScope.user.username}"/></p>
                        <a href="dashboard/album/new" class="btn btn-primary">
                            <img src="files/icons/plus.svg"/> Nieuw album
                        </a>

                        <button class="btn btn-warning selection-toggler" onclick="toggleSelectionMode()">
                            <img src="files/icons/check-square.svg"/> Selecteer albums
                        </button>

                        <div class="selection-mode-menu">                           
                            <button class="btn btn-danger" onclick="startDeletion()">
                                <img src="files/icons/trash.svg"/> Selectie verwijderen
                            </button>
                        </div>
                    </header>

                    <c:if test="${not empty successMessage}">
                        <div class="infoMessage success">
                            <p><c:out value="${successMessage}"/></p>
                        </div>
                    </c:if>

                    <div id="dashboard-albums">
                        <p>U heeft nog geen fotoalbums. Nu lijkt een goed moment er één te maken, vind u niet?</p>
                    </div>
                </c:when>
                <c:otherwise>
                    <div class="content-warning">
                        <img src="files/photos/static/illus_lock.png" title="Lock by Rudityas for GlazeStock"/>
                        <p>U heeft geen toegang tot deze pagina. Meld u eerst aan.</p>
                    </div>
                </c:otherwise>
            </c:choose>

        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="files/js/bootstrap.min.js"></script>
        <script src="files/js/assemblages.js"></script>
        <script>
                                $(document).ready(function () {
                                    getDashboardAlbums(<c:out value="${sessionScope.user.id}"/>);
                                });

                                let selectedItemIds = [];
                                let selectionModeStatus = false;
                                const SELECTION_MENU = document.querySelector(".selection-mode-menu");
                                const SELECTION_BTN = document.querySelector(".selection-toggler");

                                function toggleSelectionMode() {
                                    if (selectionModeStatus) {
                                        resetSelectionMode();
                                    } else {
                                        startSelectionMode();
                                    }
                                }

                                function startSelectionMode() {
                                    SELECTION_BTN.innerHTML = `<img src="files/icons/check.svg"/> Stop selecteren`;
                                    SELECTION_MENU.style.display = "inline-block";
                                    addEventListenersToAlbums();
                                    selectionModeStatus = true;
                                }

                                function resetSelectionMode() {
                                    selectedItemIds = [];
                                    SELECTION_BTN.innerHTML = `<img src="files/icons/check-square.svg"/> Selecteer albums`;
                                    SELECTION_MENU.style.display = "none";
                                    removeEventListenersFromAlbums();
                                    selectionModeStatus = false;
                                }

                                function startDeletion() {
                                    if (selectedItemIds.length > 0) {
                                        if (confirm("Opgelet: u staat op het punt om " + selectedItemIds.length + " album(s) definitief te verwijderen. Druk op 'Ok' om door te gaan en de geselecteerde items te verwijderen.")) {
                                            deleteAlbumsInIdArray(selectedItemIds);
                                        }
                                    }

                                }

                                function addEventListenersToAlbums() {
                                    let elems = document.getElementsByClassName("album");
                                    for (let i = 0; i < elems.length; i++) {
                                        elems[i].setAttribute("data-selected", "false");

                                        elems[i].addEventListener("click", onAlbumSelected);
                                    }
                                }

                                function onAlbumSelected() {
                                    if (this.getAttribute("data-selected") === "false") {
                                        this.style.border = "solid 2px #41c3f2";
                                        selectedItemIds.push(this.getAttribute("id"));
                                        this.setAttribute("data-selected", "true");
                                    } else {
                                        this.style.border = null;
                                        selectedItemIds.splice(selectedItemIds.indexOf(this.getAttribute("id")));
                                        this.setAttribute("data-selected", "false");
                                    }
                                }

                                function removeEventListenersFromAlbums() {
                                    let elems = document.getElementsByClassName("album");
                                    for (let i = 0; i < elems.length; i++) {
                                        elems[i].style.border = null;
                                        elems[i].removeAttribute("data-selected");
                                        elems[i].removeEventListener("click", onAlbumSelected);
                                    }
                                }
        </script>
    </body>
</html>