function openLoader(elementId){
    let e = document.getElementById(elementId);
    e.style.display = "inline-block";
    e.removeEventListener("transitionend");
    e.classList.add("appearing");
    e.classList.remove("disappearing");
}

function closeLoader(elementId){
    let e = document.getElementById(elementId);
    e.classList.remove("appearing");
    e.classList.add("disappearing");
    setTimeout(function(){
        e.style.display = "none";
    }, 1000);
}

async function getDashboardAlbums(id){
    let resp = await fetch("async?method=dashboardAlbums&params=" + id);
    let html = await resp.text();
    document.getElementById('dashboard-albums').innerHTML = html;
    let elems = document.getElementsByClassName('album');
    
    for(let i = 0; i < elems.length; i++){
        let idAttr = elems[i].getAttribute('id');
        let id = idAttr.split("-")[1];
        getAlbumPreviewPhoto(id);
    }
    
    closeLoader("db-loader");
}

async function getAlbumPreviewPhoto(id){
    let resp = await fetch("async?method=albumPreviewPhoto&params=" + id);
    let value = await resp.text();
    let e = document.getElementById('album-' + id).getElementsByClassName('cover-image')[0];
    console.log(e);
    console.log(value);
    e.style.backgroundImage = 'url('+value+')';
    e.style.backgroundSize = "cover";
}

async function generateUniqueLink(elem){
    let resp = await fetch("../../async?method=generateUniqueLink");
    let value = await resp.text();
    elem.value = value;
}

async function deleteAlbumsInIdArray(arr){
    let idArray = [];
    for(let i = 0; i < arr.length; i++){
        idArray.push(arr[i].split("-")[1]);
    }
    
    let resp = await fetch("async?method=deleteAlbums&params=" + idArray.join());
    let value = await resp.text();
    
    for(let i = 0; i < arr.length; i++){
        document.getElementById(arr[i]).remove();
    }
    
    alert("Voltooid. " + parseInt(value) + " item(s) werd(en) verwijderd.");
}