<%-- 
    Document   : index
    Created on : Feb 4, 2020, 1:30:39 PM
    Author     : MEOBL79
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Assemblages - Deel fotoalbums met uw klanten</title>
        <link rel="stylesheet" href="files/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="files/css/datepicker.min.css"/>
    </head>
    <body>
        <c:import url="/WEB-INF/imports/header.jsp"/>

        <div class="container">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="files/photos/static/women_standing.jpg" alt="Women standing">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="files/photos/static/man_stairs.jpg" alt="Man at stairs">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="files/photos/static/woman_sitting.jpg" alt="Woman sitting">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="files/photos/static/man_standing.jpg" alt="Man standing">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="files/photos/static/women_on_rock.jpg" alt="Women on a rock">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

            <article>
                <h1>Assemblages</h1>
                <p>Deel uw professionele fotoalbums met uw modellen.</p>
            </article>


            <div class="row">
                <div class="col-sm-6">
                    <h2>Publiceer uw foto's in originele kwaliteit.</h2>
                    <p>Maak uw foto's beschikbaar voor uw modellen. In hun originele kwaliteit, altijd en overal beschikbaar.</p>
                </div>
                <div class="col-sm-6">
                    <img src="files/photos/static/illus_camera.png"
                         title="Retro Film Camera, by Rudityas. Source: Glazestock."
                         alt="Illustration of a retro film camera on a blue background."/>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <img src="files/photos/static/illus_woman_computer.png" 
                         title="Woman working on computer, by PAWEL. Source: Glazestock." 
                         alt="Illustration of a woman drawn in purple, working on a laptop. Floating circles and squares can be seen floating around the laptop."/>
                </div>
                <div class="col-sm-6">
                    <h2>Upload waar u ook bent, download overal.</h2>
                    <p>U kunt uw foto's waar u zich ook bevindt uploaden en uw fotoalbums beheren. Uw modellen
                        kunnen de foto's en albums downloaden waar en wanneer zij willen.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <h2>Laat de wereld uw werk zien en bereik nieuwe klanten.</h2>
                    <p>Maak uw foto's publiek en laat de wereld uw foto's zien. Op deze manier vergroot u uw bereik
                        en ontdekken nieuwe potentiële klanten u.</p>
                </div>
                <div class="col-sm-6">
                    <img src="files/photos/static/illus_people_reading.png"
                         title="Verify, by Anonymous. Source: Glazestock."
                         alt="Illustration of two people, a woman holding a laptop, and a man holding a smartphone, standing next to each other. 
                         Text bubbles next to their head indicate that they are talking. A checkmark in a green circle indicates that they agree 
                         with what they are saying."/>
                </div>
            </div>

            <div class="standout">
                <h2>Meld u aan en begin meteen te delen.</h2>

                
            </div>

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="files/js/bootstrap.min.js"></script>
    </body>
</html>
