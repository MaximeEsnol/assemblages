package exceptions;

public class AssemblagesException extends Exception  {
	public AssemblagesException(String message) {
		super(message);
	}
}
