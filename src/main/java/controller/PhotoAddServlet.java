/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Album;
import exceptions.AssemblagesException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.DigestException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.codec.digest.DigestUtils;
import utils.Generators;

@WebServlet(name = "PhotoAddServlet", urlPatterns = "/dashboard/photos/add")
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 10, maxFileSize = 1024 * 1024 * 50, maxRequestSize = 1024 * 1024 * 100)
public class PhotoAddServlet extends PhotoManagementServlet {

    ServletContext ctx;
    private static final String UPLOAD_DIR = "files" + File.separator + "photos" + File.separator + "custom" + File.separator;
    private static final String[] ALLOWED_FILE_TYPES = {"jpeg", "jpg", "png", "gif"};

    @Override
    public void init() {
        ctx = this.getServletContext();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        session = req.getSession();

        try {
            Album album = getAlbumIfUserAllowed(req, resp);

            String applicationPath = this.getServletContext().getRealPath("");
            String uploadFilePath = applicationPath + File.separator + UPLOAD_DIR;

            File fileSaveDir = new File(uploadFilePath);
            if (!fileSaveDir.exists()) {
                fileSaveDir.mkdir();
            }
            
            PrintWriter out = resp.getWriter();

            for (Part part : req.getParts()) {
                String fileExtension = getFileExtension(part);
                
                if (inArray(ALLOWED_FILE_TYPES, fileExtension)) {
                    long time = System.currentTimeMillis();
                    String randomTextAfterFileTime = Generators.getRandomString(10);
                    String fileName = DigestUtils.md5Hex(Long.toString(time)) + randomTextAfterFileTime + "." + fileExtension;
                    part.write(UPLOAD_DIR + fileName);
                }
            }

            //resp.sendRedirect("../../dashboard?message=photosUploaded");
            
        } catch (IllegalAccessException ex) {
            ctx.getRequestDispatcher("/WEB-INF/noaccess.jsp").forward(req, resp);
        } catch (SQLException | AssemblagesException | NumberFormatException ex) {
            ctx.getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        session = req.getSession();

        try {
            Album album = getAlbumIfUserAllowed(req, resp);
            req.setAttribute("album", album);
            ctx.getRequestDispatcher("/WEB-INF/album_add_photos.jsp").forward(req, resp);
        } catch (IllegalAccessException e) {
            ctx.getRequestDispatcher("/WEB-INF/noaccess.jsp").forward(req, resp);
        } catch (SQLException | AssemblagesException | NumberFormatException ex) {
            ctx.getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
        }
    }

    private String getFileName(Part file) {
        String partHeader = file.getHeader("content-disposition");
        for (String s : partHeader.split(";")) {
            if (s.trim().startsWith("filename")) {
                return s.substring(
                        s.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    private String getFileExtension(Part file) {
        String name = getFileName(file);
        String[] parts = name.split(".");
        return parts[parts.length - 1];
    }

    private boolean inArray(String[] array, String str) {
        for (String s : array) {
            if (s.equals(str)) {
                return true;
            }
        }
        return false;
    }

}
