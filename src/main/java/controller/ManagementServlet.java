/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Album;
import beans.User;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import interfaces.UserDB;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.Objects;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import proxy.AlbumProxy;
import proxy.UserProxy;

@WebServlet(name = "ManagementServlet")
public class ManagementServlet extends HttpServlet{
    
    HttpSession session;
    
    protected Album getAlbumIfUserAllowed(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, IllegalAccessException, SQLException, AssemblagesException {
        if (session == null) {
            session = req.getSession();
        }
        
        AlbumDB albumDB = new AlbumProxy();
        UserDB userDB = new UserProxy();
        
        if (req.getParameter("album") != null) {
            if (session.getAttribute("user") != null) {
                User user = (User) session.getAttribute("user");
                int albumId = Integer.parseInt(req.getParameter("album"));
                Album album = albumDB.getById(albumId);
                if (album != null) {
                    if (Objects.equals(album.getOwner().getId(), user.getId())) {
                        return album;
                    } else {
                        throw new IllegalAccessException();
                    }
                } else {
                    throw new InvalidObjectException("");
                }
            } else {
                throw new IllegalAccessException();
            }
        } else {
            throw new InvalidParameterException();
        }
    }
}
