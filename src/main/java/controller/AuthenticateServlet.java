/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.User;
import enums.SearchType;
import exceptions.AssemblagesException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import proxy.UserProxy;
import utils.Password;

/**
 *
 * @author MEOBL79
 */
@WebServlet(name = "AuthenticateServlet", urlPatterns = "/authenticate")
public class AuthenticateServlet extends HttpServlet {

    protected void process(HttpServletRequest req, HttpServletResponse resp) {
        try {
            this.getServletContext().getRequestDispatcher("/WEB-INF/authenticate.jsp").forward(req, resp);
        } catch (ServletException ex) {
            Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("confirmRegister") != null) {
            proceedWithRegistration(req, resp);
        } else if (req.getParameter("confirmSignIn") != null) {
            proceedWithSignIn(req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("message") != null) {
            selectMessageToDisplay(req.getParameter("message"), req, resp);
        } else if (req.getParameter("signout") != null) {
            proceedWithSignOut(req, resp);
        } else {
            process(req, resp);
        }
    }

    private void selectMessageToDisplay(String message, HttpServletRequest req, HttpServletResponse resp) {
        switch (message) {
            case "email_wrong":
                req.setAttribute("infoMessage", "Er werd geen Assemblages-account gevonden met dit email adres.");
                break;
            case "pw_wrong":
                req.setAttribute("infoMessage", "Het wachtwoord voor dit Assemblages-account is fout.");
                break;
        }

        process(req, resp);
    }

    private void proceedWithRegistration(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("registerEmail") != null
                && req.getParameter("registerUsername") != null
                && req.getParameter("registerPassword") != null
                && req.getParameter("birthdate") != null) {
            User user = new User();
            user.setUsername(req.getParameter("registerUsername"));
            user.setEmail(req.getParameter("registerEmail"));
            user.setPassword(req.getParameter("registerPassword"));
            LocalDate birthdate = LocalDate.parse(req.getParameter("birthdate"));
            user.setBirthdate(birthdate);

            UserProxy userDB = new UserProxy();
            try {
                userDB.create(user);
                List<User> loggedInUser = userDB.searchByUsername(user.getUsername(), SearchType.EXACT);
                startSession(req, resp, loggedInUser.get(0));
            } catch (SQLException ex) {
                Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (AssemblagesException ex) {
                Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
                req.setAttribute("infoMessage", ex.getMessage());
                process(req, resp);
            }
        }
    }

    private void startSession(HttpServletRequest req, HttpServletResponse resp, User user) {
        HttpSession session = req.getSession();
        session.setAttribute("user", user);
        try {
            resp.sendRedirect("dashboard");
        } catch (IOException ex) {
            Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void proceedWithSignIn(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("email") != null
                && req.getParameter("password") != null) {
            UserProxy userDB = new UserProxy();
            try {
                List<User> loggedInUser = userDB.searchByEmail(req.getParameter("email"));

                if (!loggedInUser.isEmpty()) {
                    if (Password.verify(req.getParameter("password"), loggedInUser.get(0).getPassword())) {
                        startSession(req, resp, loggedInUser.get(0));   
                    } else {
                        req.setAttribute("infoMessage", "Het wachtwoord voor dit Assemblages-account is fout.");
                        process(req, resp);
                    }
                } else {
                    req.setAttribute("infoMessage", "Er werd geen Assemblages-account gevonden met dit email adres.");
                    process(req, resp);
                }
                

            } catch (SQLException ex) {
                Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (AssemblagesException ex) {
                Logger.getLogger(AuthenticateServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void proceedWithSignOut(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("signout") != null
                && req.getSession().getAttribute("user") != null) {
            req.getSession().removeAttribute("user");
            req.setAttribute("successMessage", "U werd afgemeld. Tot de volgende keer.");
        }
        process(req, resp);
    }

}
