/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Album;
import beans.User;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import interfaces.UserDB;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import proxy.AlbumProxy;
import proxy.UserProxy;
import utils.Generators;

/**
 *
 * @author MEOBL79
 */
public class AlbumManagementServlet extends ManagementServlet {

    protected Album currentAlbum;
    protected String errorRedirectionUrl;

    protected void setErrorRedirectionUrl(String errorRedirectionUrl) {
        this.errorRedirectionUrl = errorRedirectionUrl;
    }

    protected void createNewAlbum(HttpServletRequest req, HttpServletResponse resp) {
        if (req.getParameter("name") != null && req.getParameter("description") != null
                && req.getParameter("publicity") != null) {
            try {
                verifyFieldValues(req, resp);
            } catch (AssemblagesException e) {
                generateError(e.getMessage(), req, resp);
            }
        } else {
            generateError("U heeft niet alle verplichte velden ingevuld. Het album kon niet worden aangemaakt.", req, resp);
        }
    }

    private void verifyFieldValues(HttpServletRequest req, HttpServletResponse resp) throws AssemblagesException {
        HttpSession session = req.getSession();
        String name = req.getParameter("name");
        String description = req.getParameter("description");
        String publicity = req.getParameter("publicity");
        String link = null;
        User owner = (User) session.getAttribute("user");
        User forUser = null;
        Integer publicityValue = null;

        try {
            publicityValue = getPublicityValue(publicity);
        } catch (IllegalArgumentException e) {
            generateError("Het album kon niet worden aangemaakt omdat de waarde van de zichtbaarheid van het album ongeldig is.", req, resp);
        }

        if (publicityValue == 0 && req.getParameter("target") != null) {
            try {
                AlbumDB albumDB = new AlbumProxy();
                UserDB userDB = new UserProxy();
                List<User> users = userDB.searchByEmail(req.getParameter("target"));

                if (users.isEmpty()) {
                    link = Generators.getUniqueLink(albumDB);
                    forUser = owner;
                } else {
                    forUser = users.get(0);
                }

            } catch (SQLException | AssemblagesException ex) {
                Logger.getLogger(AlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (publicityValue == 0 && req.getParameter("target") == null) {
            throw new AssemblagesException("U moet een persoon voor wie dit album bedoelt is opgeven wanneer u het fotoalbum privé maakt.");
        } else if (publicityValue == 1 && req.getParameter("link") != null) {
            link = req.getParameter("link");
            forUser = owner;
        } else if (publicityValue == 1 && req.getParameter("link") == null) {
            throw new AssemblagesException("U moet een link laten genereren wanneer u het fotoalbum onzichtbaar maakt");
        } else if (publicityValue == 2) {
            forUser = owner;
        }

        currentAlbum = createAlbum(name, description, owner, forUser, publicityValue, link, req, resp);
    }

    private Integer getPublicityValue(String publicity) {
        Integer publicityValue = 0;
        switch (publicity) {
            case "private":
                publicityValue = 0;
                break;
            case "withlink":
                publicityValue = 1;
                break;
            case "public":
                publicityValue = 2;
                break;
            default:
                throw new IllegalArgumentException();
        }

        return publicityValue;
    }

    private Album createAlbum(String name, String description, User owner, User forUser, Integer publicityValue, String link, HttpServletRequest req, HttpServletResponse resp) {
        Album album = new Album();
        album.setName(name);
        album.setDescription(description);
        album.setOwner(owner);
        album.setForUser(forUser);
        album.setPublicity(publicityValue);
        album.setLink(link);
        album.setCreationDate(LocalDate.now());
        return album;
    }

    protected void uploadAlbum(Album album, HttpServletRequest req, HttpServletResponse resp) {
        try {
            AlbumDB albumDB = new AlbumProxy();
            UserDB userDB = new UserProxy();
            albumDB.create(album);
            resp.sendRedirect("../../dashboard?message=albumCreated");
        } catch (SQLException | AssemblagesException ex) {
            generateError("Er trad een onbekende fout op tijdens het aanmaken van het fotoalbum. Probeer het later opnieuw.", req, resp);
        } catch (IOException ex) {
            generateError("Het album werd toegevoegd, maar Assemblages kon u niet omleiden naar uw Dashboard.", req, resp);
        }
    }

    protected void editAlbum(Album album, int id, HttpServletRequest req, HttpServletResponse resp) {
        try {
            AlbumDB albumDB = new AlbumProxy();
            UserDB userDB = new UserProxy();
            albumDB.edit(album, id);
            resp.sendRedirect("../../dashboard?message=albumEdited");
        } catch (SQLException | AssemblagesException ex) {
            generateError("Er trad een onbekende fout op tijdens het bewerken van het fotoalbum. Probeer het later opnieuw.", req, resp);
        } catch (IOException ex) {
            generateError("Het album werd bewerkt, maar Assemblages kon u niet omleiden naar uw Dashboard.", req, resp);
        }
    }

    protected void generateError(String error, HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("infoMessage", error);
        try {
            this.getServletContext().getRequestDispatcher(errorRedirectionUrl).forward(req, resp);
        } catch (ServletException | IOException ex) {
            Logger.getLogger(AlbumServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
