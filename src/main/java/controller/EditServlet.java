/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.Album;
import beans.User;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import interfaces.UserDB;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.PrintWriter;
import java.security.InvalidParameterException;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import proxy.AlbumProxy;
import proxy.UserProxy;

/**
 *
 * @author MEOBL79
 */
@WebServlet(name = "EditServlet", urlPatterns = {"/dashboard/album/edit"})
public class EditServlet extends AlbumManagementServlet {

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        session = req.getSession();

        try {
            Album album = getAlbumIfUserAllowed(req, resp);
            req.setAttribute("album", album);
            this.getServletContext().getRequestDispatcher("/WEB-INF/album_edit.jsp").forward(req, resp);
        } catch (IllegalAccessException ex) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/noaccess.jsp").forward(req, resp);
        } catch (SQLException | AssemblagesException | InvalidParameterException | InvalidObjectException ex) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        session = req.getSession();
        
        AlbumDB albumDB = new AlbumProxy();
        UserDB userDB = new UserProxy();

        if(session.getAttribute("user") != null){
            User user = (User) session.getAttribute("user");
            
            if(req.getParameter("id") != null){
                try{
                    int albumId = Integer.parseInt(req.getParameter("id"));
                    Album album = albumDB.getById(albumId);
                    
                    if(album.getOwner().getId() == user.getId()){
                        this.setErrorRedirectionUrl("/WEB-INF/album_edit.jsp");
                        createNewAlbum(req, resp);
                        editAlbum(currentAlbum, albumId, req, resp);
                    } else {
                        this.getServletContext().getRequestDispatcher("/WEB-INF/noaccess.jsp").forward(req, resp);
                    }
                } catch (NumberFormatException | SQLException | AssemblagesException ex) {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/error.jsp").forward(req, resp);
                }
            }
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
