/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import proxy.AlbumProxy;
import proxy.UserProxy;

@WebServlet(name = "AlbumServlet", urlPatterns = "/dashboard/album/new")
public class AlbumServlet extends AlbumManagementServlet {

    @Override
    public void init() throws ServletException {
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        HttpSession session = req.getSession();

        if (session.getAttribute("user") != null) {
            this.setErrorRedirectionUrl("/WEB-INF/album_add.jsp");
            createNewAlbum(req, resp);
            uploadAlbum(currentAlbum, req, resp);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        if (session.getAttribute("user") != null) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/album_add.jsp").forward(req, resp);
        }
    }
}
