/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.User;
import dao.UserDAO;
import exceptions.AssemblagesException;
import interfaces.UserDB;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import proxy.UserProxy;

/**
 *
 * @author MEOBL79
 */

@WebServlet(name = "TestServlet", urlPatterns = "/home")
public class TestServlet extends HttpServlet {
    
    private UserDB userDB;

    @Override
    public void init() throws ServletException {
        userDB = new UserProxy();
    }   
    
    protected void process(HttpServletRequest req, HttpServletResponse resp, String url) throws ServletException, IOException {
         this.getServletContext().getRequestDispatcher(url).forward(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("id") != null){
            try {
                int id = Integer.parseInt(req.getParameter("id"));
                User user = userDB.getById(id);
                req.setAttribute("user", user);
            } catch (SQLException ex) {
                Logger.getLogger(TestServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (AssemblagesException ex) {
                Logger.getLogger(TestServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if(req.getParameter("added") != null){
            req.setAttribute("added", "true");
        }
        
        process(req, resp, "/index.jsp");       
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(req.getParameter("createUser") != null){
            if(req.getParameter("username") != null
            && req.getParameter("email") != null
            && req.getParameter("birthdate") != null){
                try {
                    LocalDate bd = LocalDate.parse(req.getParameter("birthdate"));
                    LocalDate rd = LocalDate.now();
                    
                    User user = new User();
                    user.setUsername(req.getParameter("username"));
                    user.setEmail(req.getParameter("email"));
                    user.setBirthdate(bd);
                    user.setRegistrationDate(rd);
                    user.setPassword("RandDOM123123");
                    userDB.create(user);
                    
                    resp.sendRedirect("home?added");
                } catch (SQLException | AssemblagesException ex) {
                    Logger.getLogger(TestServlet.class.getName()).log(Level.SEVERE, null, ex);
                    req.setAttribute("error", ex.getMessage());
                    process(req, resp, "/index.jsp");
                    
                }
            }
        }
    }
}
