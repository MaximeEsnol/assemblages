package controller;

import beans.Album;
import beans.Photo;
import beans.User;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import interfaces.PhotoDB;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import proxy.AlbumProxy;
import proxy.PhotoProxy;
import utils.Generators;

@WebServlet(name = "AsyncServlet", urlPatterns = "/async")
public class AsyncServlet extends HttpServlet {

    private HttpSession sess;

    @Override
    public void init() throws ServletException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.sess = req.getSession();

        if (req.getParameter("method") != null) {
            switch (req.getParameter("method")) {
                case "dashboardAlbums":
                    showDashboardAlbums(req, resp);
                    break;
                case "albumPreviewPhoto":
                    showAlbumPreviewPhotoPath(req, resp);
                    break;
                case "generateUniqueLink":
                    generateUniqueLink(req, resp);
                    break;
                case "deleteAlbums":
                    deleteAlbumsInArray(req, resp);
                    break;
            }
        }
    }

    private void showDashboardAlbums(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("params") != null) {
            int id = Integer.parseInt(req.getParameter("params"));

            if (sess.getAttribute("user") != null) {
                User currentUser = (User) sess.getAttribute("user");

                if (currentUser.getId() == id) {
                    try {
                        AlbumDB albumDB = new AlbumProxy();
                        List<Album> albums = albumDB.getFromUser(id);
                        req.setAttribute("albums", albums);
                        this.getServletContext().getRequestDispatcher("/WEB-INF/async/dashboardAlbums.jsp").forward(req, resp);
                    } catch (SQLException | AssemblagesException ex) {
                        Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }

        }
    }

    private void showAlbumPreviewPhotoPath(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("params") != null) {
            int id = Integer.parseInt(req.getParameter("params"));

            if (sess.getAttribute("user") != null) {
                User currentUser = (User) sess.getAttribute("user");
                try {
                    PhotoDB photoDB = new PhotoProxy();
                    Photo photo = photoDB.getFirstFromAlbumFromUser(id, currentUser.getId());
                    req.setAttribute("photoPath", photo.getPath());
                    this.getServletContext().getRequestDispatcher("/WEB-INF/async/albumCoverPhoto.jsp").forward(req, resp);
                } catch (SQLException ex) {
                    Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                } catch (AssemblagesException ex) {
                    Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void generateUniqueLink(HttpServletRequest req, HttpServletResponse resp) {
        if (sess.getAttribute("user") != null) {
            User currentUser = (User) sess.getAttribute("user");
            AlbumDB albumDB = new AlbumProxy();
            req.setAttribute("link", Generators.getUniqueLink(albumDB));
            try {
                this.getServletContext().getRequestDispatcher("/WEB-INF/async/uniqueLink.jsp").forward(req, resp);
            } catch (ServletException | IOException ex) {
                Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void deleteAlbumsInArray(HttpServletRequest req, HttpServletResponse resp) {
        if (sess.getAttribute("user") != null) {
            User currentUser = (User) sess.getAttribute("user");
            if (req.getParameter("params") != null) {
                String[] ids;
                if(req.getParameter("params").contains(",")){
                    ids = req.getParameter("params").split(",");
                } else {
                    ids = new String[]{req.getParameter("params")};
                }
                
                int deletedItems = 0;
                for (String i : ids) {
                    Integer id = Integer.parseInt(i);
                    try {
                        AlbumDB albumDB = new AlbumProxy();
                        Album album = albumDB.getById(id);
                        if (Objects.equals(album.getOwner().getId(), currentUser.getId())) {
                            albumDB.delete(album);
                            deletedItems++;
                        }
                    } catch (SQLException | AssemblagesException ex) {
                        Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                req.setAttribute("deletedItems", deletedItems);
                try {
                    this.getServletContext().getRequestDispatcher("/WEB-INF/async/deletedItems.jsp").forward(req, resp);
                } catch (ServletException | IOException ex) {
                    Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
