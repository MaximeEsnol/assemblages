package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionManager {
    
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String URL = "jdbc:mysql://localhost:3306/photography";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static Connection con = null;
    
    private ConnectionManager() {}
    
    public static Connection create() throws SQLException{
        if(con != null){
            return con;
        } else {
                if(con == null || con.isClosed()){
                    try {
                        Class.forName(DRIVER);
                        con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
                        return con;
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }
        return null;
    }
    
    public static void close(){
        try{
            con.close();
            con = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeAll(ResultSet rs, PreparedStatement ps){
        try{
            con.close();
            rs.close();
            ps.close();
            con = null;
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
