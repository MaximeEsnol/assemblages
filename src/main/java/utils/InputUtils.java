package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;

import beans.Album;
import beans.Photo;
import beans.User;
import enums.SearchType;
import exceptions.AssemblagesException;
import java.time.ZoneId;

import interfaces.*;
import java.sql.SQLException;
import java.util.List;
import proxy.*;

public class InputUtils {
	
	public static boolean verifyUser(User user) throws AssemblagesException, SQLException {
            if(verifyUsername(user.getUsername())){
                if(verifyPassword(user.getPassword())){
                    if(verifyEmail(user.getEmail())){
                        if(verifyBirthdate(user.getBirthdate())){
                            if(verifyExistingUser(user.getUsername())){
                                if(verifyExistingEmail(user.getEmail())){
                                    return true;
                                } else {
                                    throw new AssemblagesException("A user with the email '" + user.getEmail() + "' already exists.");
                                }
                            } else {
                                throw new AssemblagesException("A user with the username '" + user.getUsername() + "' already exists.");
                            }
                        } else {
                            throw new AssemblagesException("You have to be at least 13 years old to get an Assemblages account.");
                        }
                    } else {
                        throw new AssemblagesException("The email address '" + user.getEmail() + "' is not recognized as a valid email address.");
                    }
                } else {
                    throw new AssemblagesException("This password does not meet the minimum password requirements. Passwords must be at least 8 characters long and contain one number and one lowercase and uppercase letter.");
                }
            } else {
                throw new AssemblagesException("The username '" + user.getUsername() + "' is invalid.");
            }
	}
        
        public static boolean verifyExistingEmail(String email) throws SQLException, AssemblagesException{
            UserDB userDB = new UserProxy();
            
            List<User> users = userDB.searchByEmail(email);
            return (users.size() == 0);
        }
        
        public static boolean verifyExistingUser(String username) throws SQLException, AssemblagesException{
            UserDB userDB = new UserProxy();
            
            List<User> users = userDB.searchByUsername(username, SearchType.EXACT);
            return (users.size() == 0);
        }
	
	public static boolean verifyAlbum(Album album) {
		return (verifyTitle(album.getName()) && verifyContent(album.getDescription()) && verifyID(album.getOwner().getId()) && verifyID(album.getForUser().getId()) && verifyDate(album.getCreationDate().toString(), "yyyy-MM-dd"));
	}
	
	public static boolean verifyPhoto(Photo photo) {
		return (verifyTitle(photo.getName()) && verifyContent(photo.getLocation()));
	}
	
	public static boolean verifyUsername(String username) {
		return username.matches("^[a-zA-Z0-9_-]{3,30}$");
	}
	
	public static boolean verifyPassword(String password) {
		return password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$");
	}
	
	public static boolean verifyEmail(String email) {
		return email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	}
	
	public static boolean verifyDate(String date, String format) {
		if(date == null) {
			return false;
		} else {
			try {
				java.util.Date d1 = new SimpleDateFormat(format).parse(date);
				return true;
			} catch(ParseException e) {
				return false;
			}
		}
	}
	
	public static boolean verifyBirthdate(LocalDate birthDate) {
		LocalDate now = LocalDate.now();
		
		Period p = Period.between(birthDate, now);
		return (p.getYears() >= 13);
	}
	
	public static boolean verifyTitle(String title) {
		if(title == null) {
			return false;
		}else {
			return (title.length() > 2 && title.length() <= 50);
		}
	}
	
	public static boolean verifyContent(String content) {
		return (content != null);
	}
	
	public static boolean verifyInteger(int intToVerify, int min, int max) {
		return (intToVerify >= min && intToVerify <= max);
	}
	
	public static boolean verifyID(int id) {
		return (id >= 0);
	}
}
