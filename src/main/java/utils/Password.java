package utils;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class Password {
    public static String hash(String password){
        return BCrypt.withDefaults().hashToString(12, password.toCharArray());
    }
    
    public static boolean verify(String password, String passwordToVerify){
        return BCrypt.verifyer().verify(password.toCharArray(), passwordToVerify).verified;
    }
}
