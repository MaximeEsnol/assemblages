package utils;

import controller.AsyncServlet;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.RandomStringUtils;

public class Generators {

    public static String getUniqueLink(AlbumDB albumDB) {
        boolean linkIsUnique = false;
        String link = null;

        while (!linkIsUnique) {
            link = RandomStringUtils.random(20, true, true);
            try {
                albumDB.getByLink(link);
            } catch (SQLException | AssemblagesException ex) {
                Logger.getLogger(AsyncServlet.class.getName()).log(Level.SEVERE, null, ex);
            } catch (javax.persistence.NoResultException e) {
                linkIsUnique = true;
            }
        }
        
        return link;
    }
    
    public static String getRandomString(int length){
        return RandomStringUtils.random(length);
    }
}
