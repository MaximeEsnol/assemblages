/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author MEOBL79
 */
public class DBConnection {
    
    EntityManagerFactory emf;
    EntityManager em;
    EntityTransaction tx;
    
    public DBConnection(){
            emf = Persistence.createEntityManagerFactory("com.realdolmen_Assemblages_war_1.0-SNAPSHOTPU");
            em = emf.createEntityManager();
            tx = em.getTransaction();
    }
    
    public EntityTransaction getTransaction(){
        return tx;
    }
    
    public EntityManager getEntityManager(){
        return em;
    }
}
