/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import beans.Albumview;
import dao.AlbumviewDAO;
import enums.Sources;
import exceptions.AssemblagesException;
import interfaces.AlbumviewDB;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author MEOBL79
 */
public class AlbumviewProxy implements AlbumviewDB {
    
    private AlbumviewDAO dao;
    
    @Override
    public void create(Albumview albumview) throws AssemblagesException {
        verify();
        
        if(albumview.getSource() < Sources.values().length){
            if(albumview.getTime() <= System.currentTimeMillis() / 1000){
                this.dao.create(albumview);
            } else {
                throw new AssemblagesException("Invalid time value.");
            }
        } else {
            throw new AssemblagesException("Invalid Source value.");
        }
    }

    @Override
    public List<Albumview> getByAlbum(Integer id) throws AssemblagesException {
        verify();
        
        if(id > 0){
            return this.dao.getByAlbum(id);
        } else {
            throw new AssemblagesException("Invalid ID");
        }
    }
    
    private void verify(){
        if(this.dao == null){
            this.dao = new AlbumviewDAO();
        }
    }
    
}
