/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proxy;

import beans.Photoview;
import dao.PhotoviewDAO;
import enums.Sources;
import exceptions.AssemblagesException;
import interfaces.PhotoviewDB;
import java.util.List;


public class PhotoviewProxy implements PhotoviewDB {
    
    private PhotoviewDAO dao;
    
    @Override
    public void create(Photoview photoview) throws AssemblagesException {
        verify();
        
        if(photoview.getTime() >= System.currentTimeMillis() / 1000){
            if(photoview.getSource() < Sources.values().length){
                dao.create(photoview);
            } else {
                throw new AssemblagesException("Invalid value for source.");
            }
        } else {
            throw new AssemblagesException("Invalid value for time.");
        }
    }

    @Override
    public List<Photoview> getByPhoto(Integer id) throws AssemblagesException {
       verify();
       
       if(id > 0){
           return dao.getByPhoto(id);
       } else {
           throw new AssemblagesException("invalid value for ID.");
       }
    }
    
    private void verify(){
        if(this.dao == null){
            this.dao = new PhotoviewDAO();
        }
    }
}
