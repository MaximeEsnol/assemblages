package proxy;

import java.sql.SQLException;

import beans.User;
import dao.UserDAO;
import enums.SearchType;
import enums.SortBy;
import exceptions.AssemblagesException;
import interfaces.UserDB;
import java.time.LocalDate;
import java.util.List;
import utils.InputUtils;

import static utils.InputUtils.*;
import utils.Password;

public class UserProxy implements UserDB {
	private UserDAO dao;
	
	public UserProxy() {
		this.dao = null;
	}

	@Override
	public User getById(int id) throws SQLException, AssemblagesException {
		verify();
		
		if(id >= 0) {
			return dao.getById(id);
		} else {
			throw new AssemblagesException("Invalid user ID. The user ID has to be 0 or greater.");
		}
	}
        
        @Override
        public List<User> searchByUsername(String username) throws SQLException, AssemblagesException {
            return searchByUsername(username, SearchType.CONTAINS);
        }
        
        public List<User> searchByUsername(String username, SearchType searchType) throws AssemblagesException{
            verify();
            
            if(username != null || username.trim().length() > 0){
                return dao.searchByUsername(username, searchType);
            } else {
                throw new AssemblagesException("Invalid username. The username was not provided or is empty.");
            }
        }
        
        @Override
        public List<User> searchByEmail(String email) throws SQLException, AssemblagesException{
            verify();
            if(email != null || email.trim().equals("")){
                if(InputUtils.verifyEmail(email)){
                    return dao.searchByEmail(email);
                } else {
                    throw new AssemblagesException("Cannot search on email address. No valid email address was provided.");
                }
            } else {
                throw new AssemblagesException("Cannot search on email address. No email address was provided.");
            }
        }
        
	@Override
	public void create(User user) throws SQLException, AssemblagesException {
		verify();
                
		if(verifyUser(user)) {
                        user.setPassword(Password.hash(user.getPassword()));
                        user.setRegistrationDate(LocalDate.now());
			dao.create(user);
		} else {
			throw new AssemblagesException("Some fields are incorrect.");
		}
	}

	@Override
	public void edit(User updatedUser, int id) throws SQLException, AssemblagesException {
		verify();
		
		if(verifyUser(updatedUser)) {
			dao.edit(updatedUser, id);
		} else {
			throw new AssemblagesException("Some fields are incorrect.");
		}
	}
	
	private void verify() {
		if(this.dao == null) {
			this.dao = new UserDAO();
		}
	}

}
