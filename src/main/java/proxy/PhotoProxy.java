package proxy;

import java.sql.SQLException;
import java.util.List;

import beans.Photo;
import dao.PhotoDAO;
import exceptions.AssemblagesException;
import interfaces.PhotoDB;
import static utils.InputUtils.*;

public class PhotoProxy implements PhotoDB {

    private PhotoDAO dao;

    public PhotoProxy() {
        this.dao = null;
    }

    @Override
    public Photo getById(int id) throws SQLException, AssemblagesException {
        verify();

        if (verifyID(id)) {
            return dao.getById(id);
        } else {
            throw new AssemblagesException("Invalid photo ID. The ID has to be 0 or greater.");
        }
    }

    @Override
    public List<Photo> getFromAlbum(int id) throws SQLException, AssemblagesException {
        verify();

        if (verifyID(id)) {
            return dao.getFromAlbum(id);
        } else {
            throw new AssemblagesException("Invalid album ID. The ID has to be 0 or greater.");
        }
    }

    @Override
    public void create(Photo photo) throws SQLException, AssemblagesException {
        verify();

        if (verifyPhoto(photo)) {
            dao.create(photo);
        } else {
            throw new AssemblagesException("Not all fields are correct.");
        }
    }

    @Override
    public void edit(Photo updatedPhoto, int id) throws SQLException, AssemblagesException {
        verify();

        if (verifyPhoto(updatedPhoto)) {
            if (id >= 0) {
                dao.edit(updatedPhoto, id);
            } else {
                throw new AssemblagesException("Invalid album ID. The ID has to be 0 or greater. ");
            }
        } else {
            throw new AssemblagesException("Not all fields are correct.");
        }
    }

    private void verify() {
        if (dao == null) {
            dao = new PhotoDAO();
        }
    }

    @Override
    public Photo getFirstFromAlbumFromUser(int albumId, int userId) throws SQLException, AssemblagesException {
       verify();
       
       if(albumId > 0 && userId > 0){
           return dao.getFirstFromAlbumFromUser(albumId, userId);
       } else {
           throw new AssemblagesException("Invalid values for IDs.");
       }
    }

    @Override
    public List<Photo> getFromAlbumFromUser(int albumId, int userId) throws SQLException, AssemblagesException {
        verify();
        
        if(albumId > 0 && userId > 0){
            return dao.getFromAlbumFromUser(albumId, userId);
        } else {
            throw new AssemblagesException("Invalid values for IDs.");
        }
    }

    @Override
    public List<Photo> getFromAlbumWithLink(String link) throws SQLException, AssemblagesException {
        verify();
        
        if(!link.trim().isEmpty()){
            return dao.getFromAlbumWithLink(link);
        } else {
            throw new AssemblagesException("Invalid album URL.");
        }
    }
}
