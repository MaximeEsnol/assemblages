package proxy;

import java.sql.SQLException;
import java.util.List;

import beans.Album;
import dao.AlbumDAO;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import static utils.InputUtils.*;

public class AlbumProxy implements AlbumDB {

    private AlbumDAO dao;

    public AlbumProxy() {
        this.dao = null;
    }

    @Override
    public Album getById(int id) throws SQLException, AssemblagesException {
        verify();

        if (id >= 0) {
            return dao.getById(id);
        } else {
            throw new AssemblagesException("Invalid album ID. The ID has to be 0 or greater.");
        }
    }
    
    @Override
    public void delete(int id) throws AssemblagesException{
        verify();
        
        if(id > 0){
            dao.delete(id);
        } else {
            throw new AssemblagesException("Invalid ID. The ID must be greater than 0.");
        }
    }
    
    @Override
    public void delete(Album album){
        verify();
        
        dao.delete(album);
    }
    
    @Override
    public List<Album> getForUser(int id) throws SQLException, AssemblagesException {
        verify();

        if (id >= 0) {
            return dao.getForUser(id);
        } else {
            throw new AssemblagesException("Invalid user ID. The ID has to be 0 or greater.");
        }
    }
    
    @Override
    public List<Album> getFromUser(int id) throws SQLException, AssemblagesException{
        verify();
        
        if(id >= 0){
            return dao.getFromUser(id);
        } else {
            throw new AssemblagesException("Invalid user ID. The ID has to be 0 or greater.");
        }
    }

    @Override
    public void create(Album album) throws SQLException, AssemblagesException {
        verify();

        if (verifyAlbum(album)) {
            dao.create(album);
        } else {
            throw new AssemblagesException("Not all fields are correct.");
        }
    }

    @Override
    public void edit(Album updatedAlbum, int id) throws SQLException, AssemblagesException {
        verify();

        if (verifyAlbum(updatedAlbum)) {
            if (verifyID(id)) {
                dao.edit(updatedAlbum, id);
            } else {
                throw new AssemblagesException("Invalid album ID. The ID has to be 0 or greater.");
            }
        } else {
            throw new AssemblagesException("Not all fields are correct.");
        }
    }

    private void verify() {
        if (dao == null) {
            dao = new AlbumDAO();
        }
    }

    @Override
    public Album getByLink(String link) throws SQLException, AssemblagesException {
        verify();
        
        if(link.trim().length() > 0){
            return dao.getByLink(link);
        } else {
            throw new AssemblagesException("Invalid URL value.");
        }
    }

}
