/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package runner;

import beans.User;
import exceptions.AssemblagesException;
import interfaces.UserDB;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Date;
import proxy.UserProxy;

/**
 *
 * @author MEOBL79
 */
public class Main {

    public static void main(String[] args) throws SQLException, AssemblagesException {
        System.out.println("test");
        UserDB userDB = new UserProxy();

        LocalDate bd = LocalDate.parse("1999-02-11");
        LocalDate rd = LocalDate.now();

        User user = new User();
        user.setUsername("max");
        user.setEmail("conard@gmail.com");
        user.setBirthdate(bd);
        user.setRegistrationDate(rd);
        user.setPassword("RandDOM123123");
        userDB.create(user);
    }

}
