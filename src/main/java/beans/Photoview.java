/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "photoviews")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Photoview.findAll", query = "SELECT p FROM Photoview p"),
    @NamedQuery(name = "Photoview.findByPhoto", query = "SELECT p FROM Photoview p WHERE p.photo.id = :id"),
    @NamedQuery(name = "Photoview.findById", query = "SELECT p FROM Photoview p WHERE p.id = :id"),
    @NamedQuery(name = "Photoview.findByTime", query = "SELECT p FROM Photoview p WHERE p.time = :time"),
    @NamedQuery(name = "Photoview.findBySource", query = "SELECT p FROM Photoview p WHERE p.source = :source")})
public class Photoview implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;

    @JoinColumn(name = "photo", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Photo photo;

    @Basic(optional = false)
    @NotNull
    @Column(name = "time")
    private long time;

    @Basic(optional = false)
    @NotNull
    @Column(name = "source")
    private int source;

    public Photoview() {
    }

    public Photoview(Integer id) {
        this.id = id;
    }

    public Photoview(Integer id, long time, int source) {
        this.id = id;
        this.time = time;
        this.source = source;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Photoview)) {
            return false;
        }
        Photoview other = (Photoview) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Photoviews[ id=" + id + " ]";
    }

}
