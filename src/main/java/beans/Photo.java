/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "photos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Photos.findAll", query = "SELECT p FROM Photo p"),
    @NamedQuery(name = "Photos.findById", query = "SELECT p FROM Photo p WHERE p.id = :id"),
    @NamedQuery(name = "Photos.findByName", query = "SELECT p FROM Photo p WHERE p.name = :name"),
    @NamedQuery(name = "Photos.findFromAlbumWithUser", query = "SELECT p FROM Photo p WHERE p.album.id = :albumId AND p.album.owner.id = :userId"),
    @NamedQuery(name = "Photos.findFromAlbumWithLink", query = "SELECT p FROM Photo p WHERE p.album.link = :link"),
    @NamedQuery(name = "Photos.findByLocation", query = "SELECT p FROM Photo p WHERE p.location = :location"),
    @NamedQuery(name = "Photos.findByAlbum", query = "SELECT p FROM Photo p WHERE p.album.id = :albumId")
})
public class Photo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    
    @Lob
    @Size(max = 65535)
    @Column(name = "path")
    private String path;
    
    @Size(max = 255)
    @Column(name = "location")
    private String location;
    
    @ManyToOne
    private Album album;

    public Photo() {
    }

    public Photo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setAlbum(Album album){
        this.album = album;
    }
    
    public Album getAlbum(){
        return album;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Photo)) {
            return false;
        }
        Photo other = (Photo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Photos[ id=" + id + " ]";
    }
    
}
