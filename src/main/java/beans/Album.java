/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "albums")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Albums.findAll", query = "SELECT a FROM Album a"),
    @NamedQuery(name = "Albums.findById", query = "SELECT a FROM Album a WHERE a.id = :id"),
    @NamedQuery(name = "Albums.findByName", query = "SELECT a FROM Album a WHERE a.name = :name"),
    @NamedQuery(name = "Albums.findByLink", query = "SELECT a FROM Album a WHERE a.link = :link"),
    @NamedQuery(name = "Albums.findByOwner", query = "SELECT a FROM Album a WHERE a.owner.id = :ownerId"),
    @NamedQuery(name = "Albums.findByDescription", query = "SELECT a FROM Album a WHERE a.description = :description"),
    @NamedQuery(name = "Albums.findByCreationDate", query = "SELECT a FROM Album a WHERE a.creationDate = :creationDate"),
    @NamedQuery(name = "Albums.findByPublicity", query = "SELECT a FROM Album a WHERE a.publicity = :publicity"),
    @NamedQuery(name = "Albums.findAllForUser", query = "SELECT a FROM Album a WHERE a.forUser.id = :forUserID")
})
public class Album implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Size(max = 50)
    @Column(name = "name")
    private String name;
    
    @Size(max = 500)
    @Column(name = "description")
    private String description;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date")
    private Date creationDate;
    
    @Column(name = "publicity")
    private Integer publicity;
    
    @JoinColumn(name = "for_user", referencedColumnName = "id")
    @ManyToOne
    private User forUser;
    
    @JoinColumn(name = "owner", referencedColumnName = "id")
    @ManyToOne
    private User owner;
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "album_id")
    private List<Photo> photos;
    
    @Column(name = "link")
    private String link;

    public Album() {
    }

    public Album(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getCreationDate() {
        return creationDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = Date.from(creationDate.atStartOfDay()
      .atZone(ZoneId.systemDefault())
      .toInstant());
    }
    
    public void setCreationDate(Date creationDate){
        this.creationDate = creationDate;
    }
    
    public Integer getPublicity() {
        return publicity;
    }

    public void setPublicity(Integer publicity) {
        this.publicity = publicity;
    }

    public User getForUser() {
        return forUser;
    }

    public void setForUser(User forUser) {
        this.forUser = forUser;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        return "Album{" + "id=" + id + ", name=" + name + ", description=" + description + ", creationDate=" + creationDate + ", publicity=" + publicity + ", forUser=" + forUser + ", owner=" + owner + ", photos=" + photos + ", link=" + link + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Album other = (Album) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

 
    
}
