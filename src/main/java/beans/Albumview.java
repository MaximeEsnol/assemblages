/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MEOBL79
 */
@Entity
@Table(name = "albumviews")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Albumview.findAll", query = "SELECT a FROM Albumview a"),
    @NamedQuery(name = "Albumview.findById", query = "SELECT a FROM Albumview a WHERE a.id = :id"),
    @NamedQuery(name = "Albumview.findByAlbum", query = "SELECT a FROM Albumview a WHERE a.album.id = :id"),
    @NamedQuery(name = "Albumview.findByTime", query = "SELECT a FROM Albumview a WHERE a.time = :time"),
    @NamedQuery(name = "Albumview.findBySource", query = "SELECT a FROM Albumview a WHERE a.source = :source")})
public class Albumview implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "album", referencedColumnName = "id")
    private Album album;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "time")
    private long time;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "source")
    private int source;

    public Albumview() {
    }

    public Albumview(Integer id) {
        this.id = id;
    }

    public Albumview(Integer id, long time, int source) {
        this.id = id;
        this.time = time;
        this.source = source;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Albumview)) {
            return false;
        }
        Albumview other = (Albumview) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Albumview[ id=" + id + " ]";
    }
    
}
