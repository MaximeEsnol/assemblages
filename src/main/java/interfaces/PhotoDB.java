package interfaces;

import java.sql.SQLException;
import java.util.List;

import beans.Photo;
import exceptions.AssemblagesException;

public interface PhotoDB {

	public Photo getById(int id) throws SQLException, AssemblagesException;
	public List<Photo> getFromAlbum(int id) throws SQLException, AssemblagesException;
        public Photo getFirstFromAlbumFromUser(int albumId, int userId) throws SQLException, AssemblagesException;
        public List<Photo> getFromAlbumFromUser(int albumId, int userId) throws SQLException, AssemblagesException;
        public List<Photo> getFromAlbumWithLink(String link) throws SQLException, AssemblagesException;
	public void create(Photo photo) throws SQLException, AssemblagesException;
	public void edit(Photo updatedPhoto, int id) throws SQLException, AssemblagesException;
	
}
