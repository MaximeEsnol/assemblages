package interfaces;

import java.sql.SQLException;
import java.util.List;

import beans.Album;
import beans.User;
import enums.SortBy;
import exceptions.AssemblagesException;

public interface AlbumDB {
	
	public Album getById(int id) throws SQLException, AssemblagesException;
	public List<Album> getForUser(int id) throws SQLException, AssemblagesException;
        public List<Album> getFromUser(int id) throws SQLException, AssemblagesException;
        public Album getByLink(String link) throws SQLException, AssemblagesException;
	public void create(Album album) throws SQLException, AssemblagesException;
	public void edit(Album updatedAlbum, int id) throws SQLException, AssemblagesException;
        public void delete(int id) throws AssemblagesException;
        public void delete(Album album);

}
