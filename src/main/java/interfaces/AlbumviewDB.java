/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import beans.Albumview;
import exceptions.AssemblagesException;
import java.util.List;

/**
 *
 * @author MEOBL79
 */
public interface AlbumviewDB {
    
    public void create(Albumview albumview) throws AssemblagesException;
    public List<Albumview> getByAlbum(Integer id) throws AssemblagesException;
    
}
