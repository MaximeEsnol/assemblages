package interfaces;

import java.sql.SQLException;

import beans.User;
import enums.SearchType;
import enums.SortBy;
import exceptions.AssemblagesException;
import java.util.List;

public interface UserDB {
	
	public User getById(int id) throws SQLException, AssemblagesException;
        public List<User> searchByUsername(String username) throws SQLException, AssemblagesException;
        public List<User> searchByUsername(String username, SearchType searchType) throws SQLException, AssemblagesException;
        public List<User> searchByEmail(String email) throws SQLException, AssemblagesException;
	public void create(User user) throws SQLException, AssemblagesException;
	public void edit(User updatedUser, int id) throws SQLException, AssemblagesException;
	
}
