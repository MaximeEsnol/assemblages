/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import beans.Photoview;
import exceptions.AssemblagesException;
import java.util.List;

/**
 *
 * @author MEOBL79
 */
public interface PhotoviewDB {
    
    public void create(Photoview photoview) throws AssemblagesException;
    public List<Photoview> getByPhoto(Integer id) throws AssemblagesException;
    
}
