package enums;

public enum SearchType {
    EXACT(0),
    STARTS_WITH(1),
    ENDS_WITH(2),
    CONTAINS(3);
    
    private int value;

    private SearchType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
