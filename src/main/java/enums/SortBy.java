package enums;

public enum SortBy {
	NAME_ASC(0),
	NAME_DESC(1),
	RANDOM(2),
	DATE(3),
	CONTENT_ASC(4),
	CONTENT_DESC(5),
	ID_ASC(6),
	ID_DESC(7);
	
	private int value;
	
	private SortBy(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
}
