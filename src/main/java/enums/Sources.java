package enums;

public enum Sources {
    SHARED_LINK(0),
    WEBSITE(1);
    
    private int value;
    
    private Sources(int value){
        this.value = value;
    }
    
    public int getValue(){
        return this.value;
    }
}
