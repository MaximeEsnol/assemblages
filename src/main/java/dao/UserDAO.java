package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import beans.User;
import enums.SearchType;
import enums.SortBy;
import interfaces.UserDB;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class UserDAO implements UserDB {
	
        private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.realdolmen_Assemblages_war_1.0-SNAPSHOTPU");;
	private final EntityManager em = emf.createEntityManager();
        private final EntityTransaction tx = em.getTransaction();
	
	public UserDAO() {}
	
	@Override
	public User getById(int id) throws SQLException {
		return em.find(User.class, id);
	}
        
        @Override
        public List<User> searchByUsername(String username) throws SQLException {
            return searchByUsername(username, SearchType.CONTAINS);
        }
        
        @Override
        public List<User> searchByUsername(String username, SearchType searchType){
            TypedQuery<User> userQuery = em.createNamedQuery("Users.findByUsername", User.class);
            String usernameParam = null;
            switch(searchType.getValue()){
                case 0:
                    usernameParam = username;
                    break;
                case 1:
                    usernameParam = username + "%";
                    break;
                case 2:
                    usernameParam = "%" + username;
                    break;
                case 3:
                    usernameParam = "%" + username + "%";
                    break;
            }
            userQuery.setParameter("username", usernameParam);
            return userQuery.getResultList();
        }
        
        @Override
        public List<User> searchByEmail(String email) throws SQLException{
            TypedQuery<User> userQuery = em.createNamedQuery("Users.findByEmail", User.class);
            userQuery.setParameter("email", email);
            return userQuery.getResultList();
        }
        
	@Override
	public void create(User user) throws SQLException {
		em.getTransaction().begin();
		em.persist(user);
                em.getTransaction().commit();
	}

	@Override
	public void edit(User updatedUser, int id) throws SQLException {
                User user = em.find(User.class, id);
		tx.begin();
                user.setUsername(updatedUser.getUsername());
                user.setEmail(updatedUser.getEmail());
                user.setPassword(updatedUser.getPassword());
                user.setBirthdate(updatedUser.getBirthdate());
                tx.commit();
	}
}
