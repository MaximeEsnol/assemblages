package dao;

import java.sql.SQLException;
import java.util.List;
import beans.Photo;
import exceptions.AssemblagesException;
import interfaces.PhotoDB;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class PhotoDAO implements PhotoDB {
	
        private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("com.realdolmen_Assemblages_war_1.0-SNAPSHOTPU");;
	private final EntityManager em = emf.createEntityManager();
        private final EntityTransaction tx = em.getTransaction();
	
	public PhotoDAO() {}
	
	@Override
	public Photo getById(int id) throws SQLException {
		return em.find(Photo.class, id);
	}

	@Override
	public List<Photo> getFromAlbum(int id) throws SQLException {
		TypedQuery<Photo> query = em.createNamedQuery("Photos.findByAlbum", Photo.class);
                query.setParameter("albumId", id);
                return query.getResultList();
	}

	@Override
	public void create(Photo photo) throws SQLException {
                em.getTransaction().begin();
                em.persist(photo);
                em.getTransaction().commit();	
	}

	@Override
	public void edit(Photo updatedPhoto, int id) throws SQLException {
                Photo photo = em.find(Photo.class, id);
                em.getTransaction().begin();
                photo.setName(updatedPhoto.getName());
                photo.setAlbum(updatedPhoto.getAlbum());
                photo.setLocation(updatedPhoto.getLocation());
                photo.setPath(updatedPhoto.getPath());
                em.getTransaction().commit();
	}

    @Override
    public Photo getFirstFromAlbumFromUser(int albumId, int userId) throws SQLException, AssemblagesException {
        TypedQuery<Photo> query = em.createNamedQuery("Photos.findFromAlbumWithUser", Photo.class);
        query.setParameter("albumId", albumId);
        query.setParameter("userId", userId);
        List<Photo> photo = query.setMaxResults(1).getResultList();
        return photo.get(0);
    }

    @Override
    public List<Photo> getFromAlbumFromUser(int albumId, int userId) throws SQLException, AssemblagesException {
        TypedQuery<Photo> query = em.createNamedQuery("Photos.findFromAlbumWithUser", Photo.class);
        query.setParameter("albumId", albumId);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    @Override
    public List<Photo> getFromAlbumWithLink(String link) throws SQLException, AssemblagesException {
        TypedQuery<Photo> query = em.createNamedQuery("Photos.findFromAlbumWithLink", Photo.class);
        query.setParameter("link", link);
        
        return query.getResultList();
    }
}
