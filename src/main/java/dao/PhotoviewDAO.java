/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Photoview;
import interfaces.PhotoviewDB;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import utils.DBConnection;

/**
 *
 * @author MEOBL79
 */
public class PhotoviewDAO implements PhotoviewDB {
    
    private EntityManager em;
    private EntityTransaction tx;
    private DBConnection db;

    public PhotoviewDAO() {
        db = new DBConnection();
        em = db.getEntityManager();
        tx = db.getTransaction();
    }

    @Override
    public void create(Photoview photoview) {
        tx.begin();
        em.persist(photoview);
        tx.commit();
    }

    @Override
    public List<Photoview> getByPhoto(Integer id) {
        TypedQuery<Photoview> query = em.createNamedQuery("Photoview.findByPhoto", Photoview.class);
        query.setParameter("id", id);
        return query.getResultList();
    }
}
