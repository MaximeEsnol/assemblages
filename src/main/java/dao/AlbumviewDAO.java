/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.Albumview;
import interfaces.AlbumviewDB;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import utils.DBConnection;

/**
 *
 * @author MEOBL79
 */
public class AlbumviewDAO implements AlbumviewDB {
    
    private DBConnection db;
    private EntityManager em;
    private EntityTransaction tx;
    
    public AlbumviewDAO(){
        this.db = new DBConnection();
        this.em = db.getEntityManager();
        this.tx = db.getTransaction();
    }
    
    @Override
    public void create(Albumview albumview) {
        tx.begin();
        em.persist(albumview);
        tx.commit();
    }

    @Override
    public List<Albumview> getByAlbum(Integer id) {
        TypedQuery<Albumview> query = em.createNamedQuery("Albumview.findByAlbum", Albumview.class);
        query.setParameter("id", id);
        return query.getResultList();
    }

}
