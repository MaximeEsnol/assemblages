package dao;

import java.sql.SQLException;
import java.util.List;

import beans.Album;
import exceptions.AssemblagesException;
import interfaces.AlbumDB;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import utils.DBConnection;

public class AlbumDAO implements AlbumDB {

    private EntityManager em;
    private EntityTransaction tx;
    private DBConnection db;

    public AlbumDAO() {
        db = new DBConnection();
        em = db.getEntityManager();
        tx = db.getTransaction();
    }

    @Override
    public Album getById(int id) {
        return em.find(Album.class, id);
    }

    @Override
    public List<Album> getForUser(int id) throws SQLException {
        TypedQuery<Album> query = em.createNamedQuery("Albums.findAllForUser", Album.class);
        query.setParameter("forUserID", id);
        return query.getResultList();
    }

    @Override
    public void create(Album album) {
        em.getTransaction().begin();
        em.persist(album);
        em.getTransaction().commit();
    }
    
    @Override
    public void delete(int id){
        delete(this.getById(id));
    }
    
    @Override
    public void delete(Album album){
        em.getTransaction().begin();
        em.remove(album);
        em.getTransaction().commit();
    }

    @Override
    public void edit(Album updatedAlbum, int id) {
        Album album = em.find(Album.class, id);
        em.getTransaction().begin();
        album.setName(updatedAlbum.getName());
        album.setDescription(updatedAlbum.getDescription());
        album.setOwner(updatedAlbum.getOwner());
        album.setForUser(updatedAlbum.getForUser());
        album.setCreationDate(updatedAlbum.getCreationDate());
        album.setPublicity(updatedAlbum.getPublicity());
        em.getTransaction().commit();
    }

    @Override
    public List<Album> getFromUser(int id) {
        TypedQuery<Album> query = em.createNamedQuery("Albums.findByOwner", Album.class);
        query.setParameter("ownerId", id);
        return query.getResultList();
    }

    @Override
    public Album getByLink(String link) throws SQLException, AssemblagesException {
        TypedQuery<Album> query = em.createNamedQuery("Albums.findByLink", Album.class);
        query.setParameter("link", link);
        return query.getSingleResult();
    }
}
